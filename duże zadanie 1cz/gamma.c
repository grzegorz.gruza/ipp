/** @file
 * Interfejs klasy przechowującej stan gry gamma
 *
 * @author Grzegorz Gruza <gg417923@mimuw.edu.pl>
 * @copyright Grzegorz Gruza
 * @date 15.04.2020
 */

#include "gamma.h"
#include <stdio.h>
#include <stdlib.h>
struct pair{
  uint32_t st;
  uint32_t nd;
};
typedef struct pair pair_t;

struct pair{
  uint32_t st;
  uint32_t nd;
};
typedef struct pair pair_t;

pair_t make_pair(uint32_t st, uint32_t nd){
  pair_t to_return;
  to_return.st = st;
  to_return.nd = nd;
  return to_return;
}

bool equal(pair_t a, pair_t b){
  if(a.st != b.st || a.nd != b.nd) return false;
  return true;
}

void fu_init(pair_t** parent, uint32_t** rank, uint32_t width, uint32_t height){
  for(uint32_t x = 0; x < width; x++){
    for(uint32_t y = 0; y < height; y++){
      parent[x][y].st = x;
      parent[x][y].nd = y;
      rank[x][y] = 0;
    }
  }
}

pair_t* fu_find(pair_t** parent, pair_t* p){
  if((parent[p->st][p->nd]).st != p->st ||
    (parent[p->st][p->nd]).nd != p->nd){
    *p = *(fu_find(parent, &(parent[p->st][p->nd])));
  }
  return p;
}

void fu_union(pair_t** parent, uint32_t** rank, pair_t* a, pair_t* b){
  pair_t par_a = *fu_find(parent, a);
  pair_t par_b = *fu_find(parent, b);
  if(rank[a->st][a->nd] < rank[b->st][b->nd]){
    parent[par_a.st][par_a.nd] = par_b;
  }
  else{
    parent[par_b.st][par_b.nd] = par_a;
  }
  if(rank[a->st][a->nd] == rank[b->st][b->nd]){
    rank[a->st][a->nd]++;
  }
}

struct gamma{
  uint32_t width; ///< szerokość planszy, liczba dodatnia
  uint32_t height; ///< wysokosć planszy, liczba dodatnia

  uint32_t players; ///< liczba graczy, liczba dodatnia
  bool out_of_game*; ///< out_of_game[g]=true jeśi gracz g odpadł z gry
  uint32_t active_player; ///< gracz, którego jest ruch
  uint64_t busy_fields*; ///< liczba pól zajętych przez gracza
  uint64_t busy_fields_all; ///< liczba pól zajętych przez wszystkich graczy

  uint32_t areas; ///< maksymalna liczba obszarów, jakie może zająć gracz
  uint32_t* used_areas ///< liczba obszarów, które zajmuje gracz

  uint32_t** board; ///< board[x][y]=g – na polu (x,y) stoi pionek gracza g
  bool* is_golden_used; ///< is_golden_used[g] – czy gracz g wykonał złoty ruch

  pair_t** parent; ///< struktura find and union
  uint32_t** rank; ///< struktura find and union
};

typedef struct gamma gamma_t;

void gamma_delete(gamma_t *g){ //MOZE JEDNAK NIE TRZEBA SPRAWDZAC CZY != NULL //trzeba
  if(g == NULL) return;
  if(g->out_of_game != NULL) free(g->out_of_game);
  if(g->used_areas != NULL) free(g->used_areas);
  if(g->busy_fields != NULL) free(g->busy_fields);
  if(g->board != NULL){
    for(int i = 0; i < g->width; i++){
      if((game->board)[i] != NULL) free((game->board)[i]);
    }
    free(g->board);
  }
  if(g->is_golden_used != NULL) free(g->is_golden_used);
  if(g->parent != NULL){
    for(int i = 0; i < g->width; i++){
      if((game->parent)[i] != NULL) free((game->parent)[i]);
    }
    free(g->parent);
  }
  if(g->rank != NULL){
    for(int i = 0; i < g->width; i++){
      if((game->rank)[i] != NULL) free((game->rank)[i]);
    }
    free(g->rank);
  }
  free(g);
}

//TERAZ ROBIMY TO
gamma_t* gamma_new(uint32_t width, uint32_t height, /*zenterować*/uint32_t players, uint32_t areas){
  if(width < 1 || height < 1 || players < 1) return NULL; //areas zawsze > 0

  gamma_t* game = malloc(sizeof(gamma_t));
  if(game == NULL) return NULL;

  game->width = width;
  game->height = height;

  game->players = players;
  game->out_of_game = calloc(1, players*sizeof(bool));
  if(game->out_of_game == NULL){
    gamma_delete(game);
    return NULL;
  }
  game->active_player = 1;

  //BUSY FIELDS
  game->busy_fields = calloc(1, players*sizeof(uint64_t));
  if(game->busy_fields == NULL){
    gamma_delete(game);
    return NULL;
  }
  game->busy_fields_all = 0;

  //AREAS
  game->areas = areas;
  game->used_areas = calloc(1, players*sizeof(uint32_t));
  if(game->used_areas == NULL){
    gamma_delete(game);
    return NULL;
  }

  //BOARD
  game->board = malloc(width*sizeof(uint32_t*));
  if(game->board == NULL){
    gamma_delete(game);
    return NULL;
  }
  for(int i = 0; i  < height; i++){
    game->board[i] = calloc(1, height*sizeof(uint32_t));
    if(game->board[i] == NULL){
      gamma_delete(game);
      return NULL;
    }
  }

  //IS_GOLDEN_USED
  game->is_golden_used = calloc(1, players*sizeof(bool))
  if(game->is_golden_used == NULL){
    gamma_delete(game);
    return NULL;
  }

  //PARENT
  game->parent = malloc(width*sizeof(pair_t*));
  if(game->parent == NULL){
    gamma_delete(game);
    return NULL;
  }
  for(int i = 0; i  < height; i++){
    game->parent[i] = calloc(1, height*sizeof(pair_t));
    if(game->parent[i] == NULL){
      gamma_delete(game);
      return NULL;
    }
  }

  //RANK
  game->rank = malloc(width*sizeof(uint32_t*));
  if(game->rank == NULL){
    gamma_delete(game);
    return NULL;
  }
  for(int i = 0; i  < height; i++){
    game->rank[i] = calloc(1, height*sizeof(uint32_t));
    if(game->rank[i] == NULL){
      gamma_delete(game);
      return NULL;
    }
  }

  return game;
}


bool gamma_move(gamma_t *g, uint32_t player, uint32_t x, uint32_t y){

  //czy parametry są ok
  if(g == NULL ||
  player != g->active_player ||
  x > g->width ||
  y > g->height ||
  g->board[x][y] != 0){
    return false;
  }

  uint32_t various_areas = 0;
  bool adheres_to_the_area = false;

  //nie zwiększa się liczba obszarów zajętych przez gracza player
  pair_t x_y_find =  find(g->parent, make_pair(x, y));

  if((x != 0 && g->board[x-1][y] == player){
    adheres_to_the_area = true;
    if(eqeal(x_y_find, fu_find(make_pair(x-1, y))) == false) various_areas++;
    fu_union(g->parent, g->rank, make_pair(x, y), make_pair(x-1, y));
  }

  if(x != g->width && g->board[x+1][y] == player){
    adheres_to_the_area = true;
    if(eqeal(x_y_find, fu_find(make_pair(x+1, y))) == false) various_areas++;
    fu_union(g->parent, g->rank, make_pair(x, y), make_pair(x+1, y));
  }

  if(y != 0 && g->board[x][y-1] == player){
    adheres_to_the_area = true;
    if(eqeal(x_y_find, fu_find(make_pair(x, y-1))) == false) various_areas++;
    fu_union(g->parent, g->rank, make_pair(x, y), make_pair(x, y-1));
  }

  if(y != g->height && g->board[x][y+1] == player)){
    adheres_to_the_area = true;
    if(eqeal(x_y_find, fu_find(make_pair(x, y+1))) == false) various_areas++;
    fu_union(g->parent, g->rank, make_pair(x, y), make_pair(x, y+1));
  }

  if(adheres_to_the_area == true){
    g->board[x][y] = player;
    g->busy_fields[player]++;
    g->busy_fields_all++;
    g->used_areas[player] -= (various_areas - 1);
    return true;
  }

  //zwiększa się liczba obszarów zajętych przez gracza player
  else{
    if(g->used_areas[player] == g->areas){
      return false;
    }
    g->board[x][y] = player;
    g->busy_fields[player]++;
    g->busy_fields_all++;
    g->used_areas[player]++;
    return true;
  }
}

uint64_t gamma_busy_fields(gamma_t *g, uint32_t player){
  if(g == NULL ||
  1 > player || player > g->players){
    return 0;
  }
  return g->busy_fields[player];
}

bool gamma_golden_possible(gamma_t *g, uint32_t player){
  if(g == NULL ||
  1 > player || player > g->players){
    return false;
  }

  if(g->busy_fields_all > g->busy_fields[player])
    return true;
  else
    return false;
}
