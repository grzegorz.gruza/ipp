#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define ASTERISK "\t"
/*-------------STRING---------------*/

struct variableString{
  char* text;
  int sizeOfText, maxSizeOfString;
};

typedef struct variableString* variableStringPointer;

variableStringPointer emptyVariableString(){
  variableStringPointer vs = malloc (sizeof(struct variableString));
  vs->text = calloc(1, 5*sizeof(char));
  vs->sizeOfText = 0;
  vs->maxSizeOfString = 4;
  return vs;
}

variableStringPointer addLetter (char letter, variableStringPointer str){
  if(str->sizeOfText == str->maxSizeOfString){
    str->maxSizeOfString = str->maxSizeOfString * 2;
    str->text = realloc(str->text,  (str->maxSizeOfString + 1) * sizeof(char));
  }
  (str->text)[str->sizeOfText]=letter;
  (str->sizeOfText)++;
  return str;
}

//byc moze niepotrzebne
int compareVariableString (struct variableString s1, struct variableString s2){
  return strcmp(s1.text, s2.text);
}

struct stringWithSon{
  char* text;
  struct stringWithSon* son;
};

//usun to.. albo i nie
typedef struct stringWithSon* swsPointer;

swsPointer newStringWithSon (char* string, swsPointer son){
  swsPointer sws = malloc(sizeof(struct stringWithSon));
  sws->text = string;
  sws->son = son;
  return sws;
}

/*-------------NODE I TREE---------------*/

struct Node;

typedef struct Node* Tree;

struct Node {
  char* value;
  Tree smaller, bigger;
  Tree deeper;
};

Tree emptyTree(){
  //Tree t = malloc(sizeof(struct Node));
  Tree t = NULL;
  return t;
}

Tree* findTree(Tree* t, swsPointer string);

Tree* findTreeInAll(Tree* t, swsPointer string){
  //printf("WOOW\n");
  Tree* checkTree = findTree(&(*t)->deeper, string->son);
  if(checkTree != NULL) return checkTree;

  checkTree = findTree(&(*t)->smaller, string);
  if(checkTree != NULL) return checkTree;

  checkTree = findTree(&(*t)->bigger, string);
  return checkTree;
}

Tree* findTree(Tree* t, swsPointer string){
  if(*t == NULL || string == NULL) return NULL;

  if(strcmp(string->text, ASTERISK) == 0) return findTreeInAll (t, string);

  int cmp = strcmp(string->text, (*t)->value);
  if (cmp == 0){
    if (string->son == NULL) return t;
    else return findTree(&(*t)->deeper, string->son);
  }
  else if(cmp < 0) return findTree(&(*t)->smaller, string);
  else return findTree(&(*t)->bigger, string);
}

void printAll(Tree t) {
  //if(string->text != NULL) t = *(findTree(&t, string));
  if (t == NULL) return;
	printAll(t->smaller);
	printf("%s\n", t->value);
	printAll(t->bigger);
}

//int check(Tree* t, swsPointer string)

Tree newNode(swsPointer string) {
	Tree t = calloc(1, sizeof(struct Node));
	t->value = string->text;
  if(string->son != NULL) t->deeper = newNode(string->son);
	return t;
}

void insertNode (Tree *t, swsPointer string){
  if (*t == NULL) {
		*t = newNode(string);
		return;
	}
  int cmp = strcmp(string->text, (*t)->value);
  if(cmp == 0)  {
    if(string->son != NULL) insertNode(&(*t)->deeper, string->son);
  }
  else if(cmp < 0) insertNode(&(*t)->smaller, string);
  else insertNode(&(*t)->bigger, string);
}

void removeAllNodes(Tree t) {
	if (t != NULL) {
		removeAllNodes(t->smaller);
		removeAllNodes(t->bigger);
    removeAllNodes(t->deeper);
	}
	free(t);
}

Tree* returnMin(Tree* t) {
  if ((*t)->smaller != NULL) return returnMin(&(*t)->smaller);
	return t;
}

void deleteNode(Tree* tree, swsPointer string) {
  if (*tree == NULL || string == NULL /*usun drugi warunek*/) return;
  if (string->text == NULL){
    removeAllNodes(*tree);
    *tree = emptyTree();
    return;
  }

  Tree* t = findTree(tree, string); //trzeba to potem ukulturalnic
  if (t == NULL) return;

  if((*t)->bigger != NULL) {
  	Tree* min = returnMin(&(*t)->bigger);
    (*t)->value = (*min)->value;
    (*t)->deeper = (*min)->deeper;

	   Tree oldTree = *min;
    (*min) = (*min)->bigger;
    free(oldTree);
  }
  else {
    Tree oldTree = *t;
    *t = (*t)->smaller;
    free(oldTree);
  }
}

char* getOneWord (char* string, unsigned int *index){
  variableStringPointer newString = emptyVariableString();
  for(; *index < strlen(string); (*index)++)
    if(isspace(string[*index]) == 0) break;

  if(*index == strlen(string)) return NULL;

  for(; *index < strlen(string); (*index)++){
    if(isspace(string[*index]) == 0) addLetter(string[*index], newString);
    else break;
  }
  for(; *index < strlen(string); (*index)++)
    if(isspace(string[*index]) == 0) break;

  return newString->text;
}

//Przenieść do sekcji stringów
void addSon (swsPointer* sws, char* sonText){
    (*sws)->son = newStringWithSon(sonText, NULL);
}
void freeStringwithSon(swsPointer sws){
  if(sws == NULL) return;
  freeStringwithSon(sws->son);
  free(sws);
}

//usun potem to
void printStr(swsPointer sws){
  if(sws == NULL) return;
  printf("%s\n", sws->text);
  printStr(sws->son);
}

int main(){
//BŁĘDY PRZY MALLOCKACH I ALLOCKACH TRZEBA WYIFOWAĆ
//mogą być same znaki białe i wtedy się wywala.. chyba juz nie

  Tree t = emptyTree();
  char letter;
  variableStringPointer string = emptyVariableString();
  unsigned int index = 0;
  int error = 0;
  int count;
  char* word;
  swsPointer swsMain = newStringWithSon(NULL, NULL);
  int pom = 1;
  Tree* helpTree;

  while(1)
  {
    error = 0;
    free(string);
    string = emptyVariableString();
    index = 0;
    swsMain = newStringWithSon(NULL, NULL);
    count = 0;

//    pom = scanf("%c", &letter);
    while ((pom = scanf("%c", &letter)) == 1){
      addLetter(letter, string);
      if(letter < 33 && isspace(letter) == 0 && (string->text)[0] != '#') {
        error = 1; /*printf("%i\n", letter);*/}
      if(letter == '\n') break;
    //  pom = scanf("%c", &letter);
  //    printf("%i\n", pom);
    }

    if(pom != 1) return 0;

    if(strlen(string->text) == 0) return 0;

    if((string->text)[strlen(string->text) - 1] != '\n')
    {error = 1;} /*printf("LOL\n");
    printf("%c\n", (string->text)[strlen(string->text) - 1]);
    printf("%lu\n", strlen(string->text) - 1);
    printf("%s\n", string->text);}*/

    if(error == 1){
      fprintf(stderr, "ERROR\n");
      continue;
    }
    if((string->text)[0] == '#'){
      //printf("komentarz\n"); //to jest do zmiany
      continue;
    }

    swsMain->text = getOneWord(string->text, &index);
    swsPointer sws = swsMain;
    while(index < strlen(string->text)){
      word = getOneWord(string->text, &index);
      if(strcmp(word, "*") == 0){
        free(word);
        word = ASTERISK;
      }
      addSon(&sws, word);
      sws = sws->son;
      count++;
    }

      if(swsMain->text == NULL) continue;

      else if(strcmp(swsMain->text, "ADD") == 0 && (1 <= count && count <= 3)){
        insertNode(&t, swsMain->son);
        printf("OK\n");
      }
        //printf("%s\n", "ADDDD");

      else if(strcmp(swsMain->text, "DEL") == 0 && count <= 3){
        deleteNode(&t, swsMain->son);
        printf("OK\n");
      }
        //printf("%s\n", "DELLLL");

      else if(strcmp(swsMain->text, "CHECK") == 0 && (1 <= count && count <= 3)) {
        if (strcmp(word, ASTERISK) == 0) fprintf(stderr, "%s\n", "ERROR");
        else if(findTree(&t, swsMain->son) != NULL) printf("YES\n");
        else printf("NO\n");
      }

      else if(strcmp(swsMain->text, "PRINT") == 0 && count <= 2) {
        if(count == 0) printAll(t);
        else {
          helpTree = findTree(&t, swsMain->son);
          if(helpTree != NULL) printAll((*helpTree)->deeper);
        }
      }

      else {
        fprintf(stderr, "%s\n", "ERROR");
      }
    }

  //TO MUSI ZOSTAĆ
  removeAllNodes(t);
}
